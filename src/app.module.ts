import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';
import { PingModule } from './ping/ping.module';
import { AuthModule } from './auth/auth.module';
import { AuctionsModule } from './auctions/auctions.module';
import { TransactionsModule } from './transactions/transactions.module';
import { BidsModule } from './bids/bids.module';
import { LoggerModule } from './logger/logger.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://doffi:Lrqv2JosIRWwLyuq@cluster0.2nkshtv.mongodb.net/subastaph'),
    AuthModule, UsersModule, PingModule, AuctionsModule, TransactionsModule, BidsModule, LoggerModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
