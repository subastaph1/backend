import { Body, Controller, Get, Param, Patch, Post, Put, Query } from '@nestjs/common';
import { AuctionsService } from './auctions.service';
import { Auction } from './auctions.schema';
import { Paginated, Params } from '../core/ApiService';

@Controller('auctions')
export class AuctionsController {
    constructor(private auctionsService: AuctionsService) {}

    @Post()
    async create(@Body() Auction: Auction) {
        return this.auctionsService.create(Auction);
    }

    @Get()
    async find(@Query() params: Params, @Query('query') query: Params, @Query('paginate') paginate: Params) {
        const data = await this.auctionsService.find(params);

        return data;
    }

    @Get(':id')
    async get(@Param('id') auctionId: string) {
        return this.auctionsService.get(auctionId);
    }

    @Patch(':id')
    async patch(@Param('id') auctionId: string, @Body() data: Auction, @Query('query') query: Params) {
        return this.auctionsService.patch(auctionId, data, query);
    }
}
