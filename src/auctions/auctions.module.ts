import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Auction, AuctionSchema } from './auctions.schema';
import { AuctionsService } from './auctions.service';
import { AuctionsController } from './auctions.controller';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Auction.name, schema: AuctionSchema }]),
    ],
    providers: [AuctionsService],
    controllers: [AuctionsController],
})
export class AuctionsModule {}
