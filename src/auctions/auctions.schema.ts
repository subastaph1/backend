import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { HydratedDocument } from "mongoose";
import { User } from "../users/users.schema";

export type AuctionDocument = HydratedDocument<Auction>;

const oneHour = 1000 * 60 * 60;

@Schema()
export class Auction {
    _id: mongoose.Schema.Types.ObjectId

    @Prop({ required: true, ref: User.name })
    user_id: mongoose.Schema.Types.ObjectId;

    @Prop({ required: true })
    title: string;

    @Prop({ required: true })
    description: string;

    @Prop({ default: '' })
    avatarUrl: string;

    @Prop({ required: true, default: 0 })
    price: number;

    @Prop({ required: true, default: Date.now() })
    startDate: Date;

    @Prop({ required: true, default: Date.now() + oneHour })
    endDate: Date;

    @Prop({ required: true, default: 'open' })
    status: 'open' | 'closed';

    @Prop({ required: true, default: Date.now() })
    createdAt?: Date;

    @Prop({ required: true, default: Date.now() })
    updatedAt?: Date;
}

export const AuctionSchema = SchemaFactory.createForClass(Auction);