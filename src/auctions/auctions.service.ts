import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Auction } from './auctions.schema';
import { FilterQuery, Model, Query } from 'mongoose';
import { NullableId, Paginated, Params, ServiceMethods } from '../core/ApiService';
import { faker } from '@faker-js/faker';

@Injectable()
export class AuctionsService implements ServiceMethods<Auction> {
    constructor(@InjectModel(Auction.name) private auctionsModel: Model<Auction>) {}

    async create(auction: Auction) {
        const avatarUrl = faker.image.urlPlaceholder({ format: 'png', height: 400, width: 400 });
        auction.avatarUrl = avatarUrl;

        return this.auctionsModel.create(auction);
    }

    async find(params: Params) {
        const { sort, limit, skip, ...filter } = params.query;
        const q = this.auctionsModel.find(filter);

        if (q.limit) {
            q.limit(limit);
        }

        const data = await q;

        if (params.paginate)
        {
            const total = await this.auctionsModel.countDocuments(filter).clone();
            return this.paginate(data, total, skip, limit);
        }

        return data;
    }

    async get(id: string) {
        return this.auctionsModel.findById(id);
    }

    async patch(id: string, data: Auction, query: FilterQuery<Auction>) {
        return this.auctionsModel.findByIdAndUpdate(id, data, query);
    }

    update(id: NullableId, data: Auction, params?: Params): Promise<Auction> {
        throw new Error('Method not implemented.');
    }
    remove(id: NullableId, params?: Params): Promise<Auction> {
        throw new Error('Method not implemented.');
    }

    paginate(data: Auction[], total: number, skip: number, limit: number): Paginated<Auction> {
        return {
            data,
            total,
            skip: skip ?? 0,
            limit: limit ?? 0,
        }
    }
}
