import { QSPipe } from './qs.pipe';
import * as qs from 'qs';

describe('QSPipe', () => {
  it('should be defined', () => {
    expect(new QSPipe()).toBeDefined();
  });

  it('should return parsed object', () => {
    const qsPipe = new QSPipe();

    const clientParamsObj = { query: { sort: { or: ['filter1', 'filter2']}}, paginate: { max: '10' }};
    const clientParamsQs = qs.stringify(clientParamsObj);

    const params = qsPipe.transform(clientParamsQs, null);

    expect(params).toEqual(clientParamsObj);
  })
});
