import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import * as qs from 'qs';

@Injectable()
export class QSPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    return qs.parse(value);
  }
}
