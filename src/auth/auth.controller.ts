import { Controller, Get, Post, UseGuards } from '@nestjs/common';
import { Request } from '@nestjs/common';

import { AuthService } from './auth.service';
import { Public } from './constants';
import { LocalAuthGuard } from './local-auth.guard';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {}

    @Post('login')
    @UseGuards(LocalAuthGuard)
    @Public()
    login(@Request() req): any {
        return this.authService.login(req.user);
    }

    @Get('profile')
    getProfile(@Request() req): any {
        return req.user;
    }

}
