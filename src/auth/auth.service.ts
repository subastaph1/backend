import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

import { UsersService } from '../users/users.service';
import { User } from 'src/users/users.schema';

@Injectable()
export class AuthService {
    constructor(private usersService: UsersService, private jwtService: JwtService) {}

    async validateUser(username: string, password: string): Promise<User> {
        const [user] = await this.usersService.find({ username, limit: 1 });

        if (user && await bcrypt.compare(password, user.password)) {
            const { password, ...result } = user;

            return result;
        }

        return null;
    }

    async login(user: User) {
        const payload = { sub: user._id, username: user.username };

        return {
            ...user,
            access_token: await this.jwtService.signAsync(payload),
        };
    }
}
