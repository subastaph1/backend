import { SetMetadata } from "@nestjs/common";

export const jwtConstants = {
    secret: 'should be not public. ok?',
}

export const IS_PUBLIC_KEY = 'IS_PUBLIC';
export const Public = () => SetMetadata(IS_PUBLIC_KEY, true);