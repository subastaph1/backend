import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { User } from '../users/users.schema';

@Injectable()
export class HashPipe implements PipeTransform {
  async transform(value: User, metadata: ArgumentMetadata): Promise<User> {
    const saltRounds = 10;

    if (value.password) {
      const hashPass = await bcrypt.hash(value.password, saltRounds);

      return Object.assign({}, value, { password: hashPass });
    }

    return value;
  }
}
