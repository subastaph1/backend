import { Body, Controller, Get, Post, Query, UseInterceptors, UsePipes } from '@nestjs/common';
import { BidsService } from './bids.service';
import { Bid } from './bids.schema';
import { OwnerPipe } from '../common/owner.pipe';
import { OwnerInterceptor } from '../common/owner.interceptor';
import { Params } from 'src/core/ApiService';

@Controller('bids')
export class BidsController {
    constructor(private bidsService: BidsService) {}

    @Post()
    async create(@Body() bid: Bid) {
        return this.bidsService.create(bid);
    }

    @Get()
    async find(@Query('query') query: Params) {
        return this.bidsService.find(query);
    }
}
