import { Module } from '@nestjs/common';
import { BidsController } from './bids.controller';
import { BidsService } from './bids.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Bid, BidSchema } from './bids.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Bid.name, schema: BidSchema}]),
  ],
  controllers: [BidsController],
  providers: [BidsService]
})
export class BidsModule {}
