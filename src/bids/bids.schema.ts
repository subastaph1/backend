import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { HydratedDocument } from "mongoose";
import { User } from "../users/users.schema";
import { Auction } from "../auctions/auctions.schema";

export type BidDocument = HydratedDocument<Bid>;

@Schema()
export class Bid {
    _id: mongoose.Schema.Types.ObjectId

    @Prop({ required: true, ref: User.name })
    user_id: mongoose.Schema.Types.ObjectId;

    @Prop({ required: true, ref: Auction.name })
    auction_id: mongoose.Schema.Types.ObjectId;

    @Prop({ required: true })
    amount: number;

    @Prop({ required: true, default: Date.now() })
    createdAt?: Date;

    @Prop({ required: true, default: Date.now() })
    updatedAt?: Date;
}

export const BidSchema = SchemaFactory.createForClass(Bid);