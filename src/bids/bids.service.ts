import { Injectable } from '@nestjs/common';
import { Bid } from './bids.schema';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { Id, NullableId, Paginated, Params, ServiceMethods } from 'src/core/ApiService';

@Injectable()
export class BidsService implements ServiceMethods<Bid> {
    constructor(@InjectModel(Bid.name) private bidsModel: Model<Bid>) {}
    
    create(bid: Bid) {
        return this.bidsModel.create(bid);
    }

    find(query: FilterQuery<Bid>): Promise<Bid | Bid[] | Paginated<Bid>> {
        return this.bidsModel.find(query);
    }

    get(id: Id, params?: Params): Promise<Bid> {
        throw new Error('Method not implemented.');
    }

    update(id: NullableId, data: Bid, params?: Params): Promise<Bid> {
        throw new Error('Method not implemented.');
    }

    patch(id: NullableId, data: Partial<Bid>, params?: Params): Promise<Bid> {
        throw new Error('Method not implemented.');
    }

    remove(id: NullableId, params?: Params): Promise<Bid> {
        throw new Error('Method not implemented.');
    }
}
