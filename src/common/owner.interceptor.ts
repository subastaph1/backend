import { CallHandler, ExecutionContext, Injectable, NestInterceptor, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { User } from 'src/users/users.schema';

@Injectable()
export class OwnerInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const req = context.switchToHttp().getRequest();

    if ('user' in req && Object.entries(req.body).length > 0) {
      const { _id: user_id }: User = req.user;

      if (req.body['user_id'] &&
      req.body['user_id'] !== user_id) {
        throw new UnauthorizedException();
      }

      req.body = Object.assign({}, req.body, { user_id });
    }

    return next.handle();
  }
}
