import { ArgumentMetadata, Inject, Injectable, PipeTransform, Scope, UnauthorizedException } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';

@Injectable({ scope: Scope.REQUEST })
export class OwnerPipe implements PipeTransform {
  constructor(@Inject(REQUEST) protected readonly req: Request | any) {}
  
  transform(value: any, metadata: ArgumentMetadata) {
    if ('user' in this.req && metadata.type === 'body') {
      const { _id: user_id } = this.req.user;
      if (value.user_id && value.user_id !== user_id) {
        throw new UnauthorizedException();
      }
      
      return Object.assign({}, value, { user_id });
    }

    return value;
  }
}
