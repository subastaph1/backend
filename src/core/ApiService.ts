export type Id = number | string;
export type NullableId = Id | null;

export interface Query {
    [key: string]: any;
}

export interface PaginationOptions {
    default: number;
    max: number;
}

export interface Params {
    query?: Query | undefined;
    paginate?: false | Pick<PaginationOptions, 'max'> | undefined;
}

export interface Paginated<T> {
    total: number;
    limit: number;
    skip: number;
    data: T[];
}

export interface ServiceMethods<T> {
    find(params?: Params): Promise<T | T[] | Paginated<T>>;
    get(id: Id, params?: Params): Promise<T>;
    create(data: Partial<T> | Array<Partial<T>>, params?: Params): Promise<T | T[]>;
    update(id: NullableId, data: T, params?: Params): Promise<T>;
    patch(id: NullableId, data: Partial<T>, params?: Params): Promise<T>;
    remove(id: NullableId, params?: Params): Promise<T>;
}