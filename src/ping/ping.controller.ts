import { Controller, Get } from '@nestjs/common';
import { Public } from '../auth/constants';

@Controller('ping')
export class PingController {

    @Public()
    @Get()
    pong(): string {
        return 'pong';
    }
}
