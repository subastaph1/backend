import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { Transaction } from './transactions.schema';
import { Params } from 'src/core/ApiService';

@Controller('transactions')
export class TransactionsController {
    constructor(private transactionsService: TransactionsService) {}

    @Post()
    async create(@Body() transaction: Transaction, @Req() req: any) {
        return this.transactionsService.create(transaction);
    }

    @Get(':id')
    async get(@Param('id') id: string, @Query('query') query: Params) {
        return this.transactionsService.get(id, query);
    }

    @Get('')
    async find(@Query('query') query: Params) {
        return this.transactionsService.find(query);
    }
}
