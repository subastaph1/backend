import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { HydratedDocument } from "mongoose";
import { User } from "../users/users.schema";
import { Auction } from "../auctions/auctions.schema";

export type TransactionDocument = HydratedDocument<Transaction>;

@Schema()
export class Transaction {
    _id: mongoose.Schema.Types.ObjectId

    @Prop({ required: true, ref: User.name })
    user_id: mongoose.Schema.Types.ObjectId;

    @Prop({ ref: Auction.name })
    auction_id?: mongoose.Schema.Types.ObjectId;

    @Prop({ required: true, default: 'deposit' })
    type: 'deposit' | 'withdrawal';

    @Prop({ required: true })
    amount: number;

    @Prop({ required: true, default: 0 })
    balance: number;

    @Prop({ required: true, default: 'open' })
    status: 'open' | 'closed';

    @Prop({ required: true, default: Date.now() })
    createdAt?: Date;

    @Prop({ required: true, default: Date.now() })
    updatedAt?: Date;

    @Prop({ default: null })
    closedAt?: Date | null;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);