import { Body, Injectable, Post } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Transaction } from './transactions.schema';
import { FilterQuery, Model } from 'mongoose';
import { NullableId, Params, Query, ServiceMethods } from '../core/ApiService';

@Injectable()
export class TransactionsService implements ServiceMethods<Transaction> {
    constructor(@InjectModel(Transaction.name) private transactionsModel: Model<Transaction>) {}
    
    async create(transaction: Transaction) {
        return this.transactionsModel.create(transaction);
    }

    async get(_id: string, query: FilterQuery<Transaction>) {
        query = Object.assign({}, query, { _id });

        return this.transactionsModel.findOne(query);
    }

    async find(queryParams: FilterQuery<Transaction>) {
        const { sort, limit, ...query } = queryParams;

        let q = this.transactionsModel.find(query);

        if (sort) {
            q.sort(sort);
        }

        if (limit) {
            q.limit(parseInt(limit));
        }

        return q.exec();
    }

    update(id: NullableId, data: Transaction, params?: Params): Promise<Transaction> {
        throw new Error('Method not implemented.');
    }
    patch(id: NullableId, data: Partial<Transaction>, params?: Params): Promise<Transaction> {
        throw new Error('Method not implemented.');
    }
    remove(id: NullableId, params?: Params): Promise<Transaction> {
        throw new Error('Method not implemented.');
    }
}
