import { Body, Controller, Post, UsePipes } from '@nestjs/common';
import { User } from './users.schema';
import { UsersService } from './users.service';
import { Public } from '../auth/constants';
import { HashPipe } from '../auth/hash.pipe';

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService) {}

    @Post()
    @UsePipes(HashPipe)
    @Public()
    async create(@Body() user: User) {
        return this.usersService.create(user);
    }
}
