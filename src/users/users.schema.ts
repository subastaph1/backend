import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { HydratedDocument } from "mongoose";

export type UserDocument = HydratedDocument<User>;

@Schema()
export class User {
    _id: mongoose.Schema.Types.ObjectId

    @Prop({ required: true, unique: true, index: true })
    username: string;

    @Prop({ required: true })
    password?: string;

    @Prop({ required: true, default: Date.now() })
    createdAt?: Date;

    @Prop({ required: true, default: Date.now() })
    updatedAt?: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);