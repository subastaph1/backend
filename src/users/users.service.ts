import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './users.schema';
import { FilterQuery, Model } from 'mongoose';
import { LoggerService } from '../logger/logger.service';
import { Id, NullableId, Params, ServiceMethods } from 'src/core/ApiService';

@Injectable()
export class UsersService implements ServiceMethods<User> {
    constructor(@InjectModel(User.name) private usersModel: Model<User>, private logger: LoggerService) {}

    async create(user: User): Promise<User> {
        return this.usersModel.create(user);
    }

    async find(query: FilterQuery<User>) {
        const { sort, limit, ...filter } = query;

        const q = this.usersModel.find(filter);

        if (sort) {
            q.sort(sort);
        }

        if (limit) {
            q.limit(parseInt(limit));
        }

        return q.exec();
    }

    get(id: Id, params?: Params): Promise<User> {
        throw new Error('Method not implemented.');
    }
    update(id: NullableId, data: User, params?: Params): Promise<User> {
        throw new Error('Method not implemented.');
    }
    patch(id: NullableId, data: Partial<User>, params?: Params): Promise<User> {
        throw new Error('Method not implemented.');
    }
    remove(id: NullableId, params?: Params): Promise<User> {
        throw new Error('Method not implemented.');
    }
}
